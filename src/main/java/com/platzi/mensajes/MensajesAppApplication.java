package com.platzi.mensajes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;
import java.util.Scanner;

@SpringBootApplication
public class MensajesAppApplication {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int opcion = 0;
		do{

			System.out.println("-------------------");
			System.out.println(" Aplicación de mensajes");
			System.out.println(" 1. crear mensaje");
			System.out.println(" 2. listar mensajes");
			System.out.println(" 3. eliminar mensaje");
			System.out.println(" 4. editar mensaje");
			System.out.println(" 5. salir");
			//leemos la opción del usuario

			opcion = sc.nextInt();

			switch (opcion){
				case 1:
					mensajesService.crearMesnaje();
					break;
				case 2:
					mensajesService.listarMensjaes();
					break;
				case 3:
					mensajesService.borrarMensaje();
					break;
				case 4:
					mensajesService.editarMensaje();
					break;
				default:
					break;
			}

		}while (opcion != 5);

	}

}
