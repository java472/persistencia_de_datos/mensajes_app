package com.platzi.mensajes;

import java.util.Scanner;

public class mensajesService {
    public static void crearMesnaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = sc.nextLine();
        System.out.println("Tu nombre");
        String nombre = sc.nextLine();

        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutoMensajes(nombre);
        MensajesDAO.crearMensajeDB(registro);

    }

    public static void listarMensjaes(){
        MensajesDAO.leerMensajeDB();
    }

    public static void borrarMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("indica el ID del mensaje a borrar");
        int idMesnaje = sc.nextInt();
        MensajesDAO.borrarMensajeDB(idMesnaje);

    }

    public static void editarMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu nuevo mensaje");
        String newMensaje = sc.nextLine();
        System.out.println("Indica el ID del mensaje a editar");
        int idMensaje = sc.nextInt();
        Mensajes actualizacion = new Mensajes();
        actualizacion.setIdMensaje(idMensaje);
        actualizacion.setMensaje(newMensaje);
        MensajesDAO.actualizarMesajeDB(actualizacion);
    }
}
