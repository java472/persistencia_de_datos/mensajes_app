package com.platzi.mensajes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mensajes {
    @JsonProperty("mensaje_id")
    private int idMensaje;
    private String mensaje;
    @JsonProperty("autor_mensaje")
    private String autorMensajes;
    @JsonProperty("fecha_mensaje")
    private String fechaMensaje;

    public  Mensajes(){

    }

    public Mensajes(String mensaje, String autoMensajes, String fechaMensaje) {
        this.mensaje = mensaje;
        this.autorMensajes = autorMensajes;
        this.fechaMensaje = fechaMensaje;
    }

    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutorMensajes() {
        return autorMensajes;
    }

    public void setAutoMensajes(String autorMensajes) {
        this.autorMensajes = autorMensajes;
    }

    public String getFechaMensaje() {
        return fechaMensaje;
    }

    public void setFechaMensaje(String fechaMensaje) {
        this.fechaMensaje = fechaMensaje;
    }
}
