package com.platzi.mensajes;

import org.springframework.boot.autoconfigure.amqp.RabbitProperties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDAO {
    public  static void crearMensajeDB(Mensajes mensaje){
        Conexion dbConnect = new Conexion();
        try (Connection conexion = dbConnect.get_connection()){
            PreparedStatement ps = null;
            try {
                String query = "INSERT INTO mensajes_app.mensajes ( mensaje, autor_mensaje) VALUES (?,?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutorMensajes());
                ps.executeUpdate();
                System.out.println("mensaje creado");
            }catch (SQLException ex){
                System.out.println(ex);
            }
        }catch (SQLException e){
            System.out.println(e);
        }
    }

    public static void leerMensajeDB(){
        Conexion dbConnect = new Conexion();
        try (Connection conexion = dbConnect.get_connection()){
            PreparedStatement ps = null;
            ResultSet rs = null;
            String query = "SELECT * FROM mensajes_app.mensajes";
            ps = conexion.prepareStatement(query);
            rs=ps.executeQuery();
            while (rs.next()){
                System.out.println("ID: " + rs.getInt("mensaje_id"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getString("fecha_mensaje"));
                System.out.println("");
            }

        }catch (SQLException e){
            System.out.println("no se puedieron recuperar los mensajes");
            System.out.println(e);
        }
    }

    public static void borrarMensajeDB(int idMensaje){
        Conexion dbConnect = new Conexion();
        try(Connection conexion = dbConnect.get_connection()) {
            PreparedStatement ps = null;
            try {
                String query = "DELETE FROM mensajes_app.mensajes WHERE (mensaje_id = ?)";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, idMensaje);
                ps.executeUpdate();
                System.out.println("El mensaje fué borrado");
            }catch (SQLException e){
                System.out.println(e);
                System.out.println("No se pudo eliminar el mensaje");
            }
        }catch (SQLException e){
            System.out.println(e);
        }
    }

    public static void actualizarMesajeDB(Mensajes mensaje){
        Conexion dbConnect = new Conexion();
        try(Connection conexion = dbConnect.get_connection()) {
            PreparedStatement ps = null;
            try {
                String query = "UPDATE mensajes_app.mensajes SET mensaje = ? WHERE (`mensaje_id` = ?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2, mensaje.getIdMensaje());
                ps.executeUpdate();
                System.out.println("El mensaje fué actualizado");
            }catch (SQLException ex){
                System.out.println(ex);
                System.out.println("No se pudo actualizar el mensaje");
            }
        }catch (SQLException e){
            System.out.println(e);
        }
    }
}
